import django_filters

from main.models import Package, Test


class PackageFilter(django_filters.FilterSet):
    class Meta:
        model = Package
        fields = {
            'name': ['contains'],
            'status': ['exact']
        }

class TestFilter(django_filters.FilterSet):
    class Meta:
        model = Test
        fields = {
            'student_index': ['contains'],
            'test_group': ['exact']
        }
