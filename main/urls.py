from main.views.template_views import template_download, template_download_view
from main.views.test_views import TestCreateView, TestDetailView, TestListView, answer_update, test_reevaluate
from main.views.package_views import PackageCreateView, PackagesListView, package_raport, \
         package_reevaluate, package_pdf
from django.urls import path
from main.one_time_startup import one_time_startup
from main.views import views
from main.views.views import token_create

urlpatterns = [
    path('', views.home, name='main-home'),
    path('register/', views.register, name='main-register'),
    path('tokenmanage/', views.manage_tokens.as_view(), name='main-manage-tokens'),
    path('tokenmanage/token/', token_create.as_view(), name='main-token'),
    path('tokenmanage/<id>/expire', views.token_expire, name='main-token-expire'),
    path('testimages/<id>/', views.test_images, name='main-test-images'),
    path('img/<id>/', views.img, name='main-img'),
    path('static/<id>/', views.static, name='main-static'),
]

package_urls = [
    path('package/new/', PackageCreateView.as_view(), name='main-create-package'),
    path('package/<int:package>/', TestListView.as_view(), name='main-package-detail'),
    path('package/<int:pk>/reevaluate/', package_reevaluate, name='main-package-reevaluate'),
    path('package/<int:pk>/download/report/', package_raport, name='main-package-raport'),
    path('package/<int:pk>/download/pdf/', package_pdf, name='main-package-pdf'),
    path('template/', template_download_view, name='main-test-template'),
    path('template/download/', template_download, name='main-test-template-download'),
    path('packages/', PackagesListView.as_view(), name='main-packages'),
    path('test/<int:pk>/',  TestDetailView.as_view(), name='main-test-detail'),
    path('test/<int:pk>/reevaluate/', test_reevaluate, name='main-test-reevaluate'),
    path('test/new/<int:pk>/', TestCreateView.as_view(), name='main-test-create'),
    path('image/<int:image_id>/answer/<int:answer_id>/update/<str:letter>/<str:value>', answer_update, name='main-answer-update'),
]
urlpatterns += package_urls

#reset_processing_packages
one_time_startup()
