
import io
from django.http import HttpRequest, FileResponse, HttpResponseForbidden
from django.shortcuts import render

def template_download_view(request):
    if request.user.is_authenticated is False:
        return HttpResponseForbidden()
    assert isinstance(request, HttpRequest)
    return render(
        request,
        'main/test_template.html'
    )

def template_download(request):
    if request.user.is_authenticated is False:
        return HttpResponseForbidden()
    buffer = io.BytesIO()
    with open("./main/static/template.pdf", "rb") as template_pdf:
        buffer.write(template_pdf.read())
        buffer.seek(io.SEEK_SET)
        return FileResponse(buffer, as_attachment=True, filename='template.pdf')
