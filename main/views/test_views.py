from src.controller.database_controller import update_test
from src.controller.appraisals_controller import appraise_test
import os
from django.views.generic import CreateView, DetailView, ListView
from main.forms import TestCreateForm
from django.conf import settings
from multiprocessing import Process
from main.models import Package, Test, ImageContainer, AnswerContainer
from src.controller.main_controller import process_pdf_single_test
from django.contrib.auth.mixins import LoginRequiredMixin, UserPassesTestMixin
from main.forms import  TestUpdateForm
from django.shortcuts import redirect, get_object_or_404
from django.contrib import messages
from django.http.response import Http404, HttpResponse, HttpResponseRedirect, HttpResponseForbidden
from main.filters import TestFilter
from django.core.paginator import Paginator
from main.forms import PackageUpdateForm



class TestCreateView(LoginRequiredMixin, CreateView):
    model = Test
    form_class = TestCreateForm
    success_url = '/package/'

    def form_valid(self, form, **kwargs):
        filename = os.path.join(settings.BASE_DIR, 'exam_pdfs', self.request.FILES['pdf'].name)
        file_path = os.path.join(settings.BASE_DIR, 'exam_pdfs')
        with open(filename, 'wb+') as destination:
            for chunk in self.request.FILES['pdf'].chunks():
                destination.write(chunk)
        package = get_object_or_404(Package, id=self.kwargs['pk'])
        package = package.cast_to_backend_object()
        src = os.path.join(file_path, self.request.FILES['pdf'].name)
        processing_process = Process(target=process_pdf_single_test, args=[src, package])
        processing_process.start()

        self.success_url += str(package.id)
        return super().form_valid(form)


class TestDetailView(LoginRequiredMixin, UserPassesTestMixin, DetailView):
    model = Test
    context_object_name = 'test'
    success_url = '/test/'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        images = []
        for image in ImageContainer.objects.filter(test=self.object):
            image_dict = {}
            image_dict['answers'] = AnswerContainer.objects.filter(img=image)
            image_dict['image'] = image
            images.append(image_dict)
        context['images'] = images
        context['possible_answers'] = ['A','B','C','D']
        context['test_update_form'] = TestUpdateForm(initial={'student_index': self.object.student_index, 'test_group': self.object.test_group, 'is_reference': self.object.is_reference})
        context['ref_tests_groups'] = [x.test_group for x in Test.objects.filter(is_reference=True, package=self.object.package)]
        # context['package_name'] = ImageContainer.objects.get(pk=self.object.package).name
        return context

    # If authorized
    def test_func(self):
        # if self.request.user.has_perm('main.create_exam'):

        if self.request.user == get_object_or_404(Test, pk=self.kwargs['pk']).package.user or get_object_or_404(Test, pk=self.kwargs['pk']).package.user is None:
            return True
        return False

    def post(self, request, pk, slug=None):
        if len(request.POST) == 1:
            response = redirect('main-package-detail', package=self.get_object().package.id)
            Test.objects.get(pk=self.get_object().id).delete()
            return response
        else:
            this_test = self.get_object()
            form = TestUpdateForm(request.POST)
            if form.is_valid():
                if this_test.student_index != form.cleaned_data['student_index']:
                    Test.objects.filter(pk=this_test.id).update(student_index=form.cleaned_data['student_index'])
                if this_test.test_group != form.cleaned_data['test_group']:
                    Test.objects.filter(pk=this_test.id).update(test_group=form.cleaned_data['test_group'])
                if this_test.is_reference != form.cleaned_data['is_reference']:
                    if form.cleaned_data['is_reference'] is True and form.cleaned_data['test_group'] in [x.test_group for x in Test.objects.filter(is_reference=True, package=Test.objects.get(id=pk).package)]:
                        messages.error(request, "Reference test already exists for group " + form.cleaned_data['test_group'])
                    else:
                        Test.objects.filter(pk=this_test.id).update(is_reference=form.cleaned_data['is_reference'])
            self.success_url += str(pk)
            next_page = request.POST.get('next', self.success_url)
            return HttpResponseRedirect(next_page)


class TestListView(LoginRequiredMixin, UserPassesTestMixin, ListView):
    model = Package
    success_url = '/package/'
    paginate_by = 15

    def get_queryset(self):
        return Test.objects.filter(package=self.kwargs['package'], is_reference=False)

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['package'] = get_object_or_404(Package, pk=self.kwargs['package'])
        try:
            context['ref_tests'] = Test.objects.filter(is_reference=True, package=self.kwargs['package'])
        except Test.DoesNotExist:
            raise Http404()
        filtered_qs = TestFilter(self.request.GET, queryset=self.get_queryset()).qs.order_by('student_index')
        context['filter'] = TestFilter(self.request.GET, queryset=filtered_qs)
        context['paginator'] = Paginator(filtered_qs, self.paginate_by)
        page = self.request.GET.get('page')
        if page is None:
            page = 1
        context['page_obj'] = context['paginator'].page(page)
        context['update_form'] = PackageUpdateForm(initial={'name': context['package'].name,
                                                            'user': context['package'].user,
                                                            'score_mode': context['package'].score_mode,
                                                            'allow_negative': context['package'].allow_negative})
        return context

    # If authorized
    def test_func(self):
        # if self.request.user.has_perm('main.create_exam'):

        if self.request.user == get_object_or_404(Package, pk=self.kwargs['package']).user or get_object_or_404(Package, pk=self.kwargs['package']).user is None:
            return True
        return False

    def post(self, request, package, slug=None):
        from main.views.package_views import reappraise_package
        package = Package.objects.get(pk=self.kwargs['package'])
        if len(request.POST) == 1:
            package.delete()
            # get_object_or_404(package.model_class(), pk=pk).delete()
            response = redirect('main-packages')
            return response
        else:
            form = PackageUpdateForm(request.POST)
            if form.is_valid():
                if package.name != form.cleaned_data['name']:
                    Package.objects.filter(pk=package.id).update(name=form.cleaned_data['name'])
                if package.user != form.cleaned_data['user']:
                    Package.objects.filter(pk=package.id).update(user=form.cleaned_data['user'])
                if package.score_mode != form.cleaned_data['score_mode']:
                    Package.objects.filter(pk=package.id).update(score_mode=form.cleaned_data['score_mode'])
                    package.score_mode = form.cleaned_data['score_mode']
                    reappraise_package(package)
                if package.allow_negative != form.cleaned_data['allow_negative']:
                    Package.objects.filter(pk=package.id).update(allow_negative=form.cleaned_data['allow_negative'])
                    package.allow_negative = form.cleaned_data['allow_negative']
                    reappraise_package(package)
            next_page = request.POST.get('next', self.success_url + str(package.id))
            return HttpResponseRedirect(next_page)




def reappraise_test(package, test):
    test.images = []
    for image in ImageContainer.objects.filter(test=test).iterator():
        image.answers = AnswerContainer.objects.filter(img=image)
        test.images.append(image)
    appraise_test(test, package)
    update_test(test)


def test_reevaluate(request, pk):
    if request.user.is_authenticated is False:
        return HttpResponseForbidden()
    test = Test.objects.get(pk=pk)
    backend_package = test.package.cast_to_backend_object()
    reappraise_test(backend_package, test)

    success_url = '/test/' + str(pk)
    next_page = request.POST.get('next', success_url)
    return HttpResponseRedirect(next_page)

def answer_update(request, image_id, answer_id, letter, value):
    if request.user.is_authenticated is False:
        return HttpResponseForbidden()
    image = ImageContainer.objects.get(id=image_id)
    answer = AnswerContainer.objects.get(img=image, answer_id=answer_id)
    current_value = set(answer.evaluated_value)
    if value == 'true':
        current_value.add(letter)
    else:
        current_value.remove(letter)
    AnswerContainer.objects.filter(pk=answer.id).update(evaluated_value=''.join(str(s) for s in sorted(current_value)))
    return HttpResponse('')
