import logging
import os
import csv
import io
from multiprocessing import Process
from django.conf import settings
from django.http import HttpResponse, FileResponse, HttpResponseForbidden, HttpResponseRedirect
from django.shortcuts import get_object_or_404
from main.models import Package, Test, ImageContainer, AnswerContainer
from main.forms import PackageCreateForm
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import CreateView
from src.test_evaluator import create_empty_package
from src.controller.main_controller import process_pdf_package
from main.filters import PackageFilter
from django.core.paginator import Paginator
from django.views.generic import ListView
from main.views.test_views import reappraise_test
from django.db.models import Q


logger = logging.getLogger('exams')


class PackageCreateView(LoginRequiredMixin, CreateView):
    model = Package
    form_class = PackageCreateForm
    success_url = '/package/'

    def get_context_data(self, **kwargs):
        context = super(CreateView, self).get_context_data(**kwargs)
        context.update({'title': "Upload filled exams"})
        return context

    def form_valid(self, form):
        filename = os.path.join(settings.BASE_DIR, 'exam_pdfs', self.request.FILES['pdf'].name)
        file_path = os.path.join(settings.BASE_DIR, 'exam_pdfs')
        with open(filename, 'wb+') as destination:
            for chunk in self.request.FILES['pdf'].chunks():
                destination.write(chunk)
        src = os.path.join(file_path, self.request.FILES['pdf'].name)
        package = create_empty_package(src, package_name=form.cleaned_data['name'])
        Package.objects.filter(pk=package.id).update(user=self.request.user)
        Package.objects.filter(pk=package.id).update(score_mode=form.cleaned_data['score_mode'])
        package.score_mode = form.cleaned_data['score_mode']
        processing_process = Process(target=process_pdf_package, args=[self.request.FILES['pdf'].name, src, package])
        processing_process.start()
        self.success_url += str(package.id)
        return super().form_valid(form)



class PackagesListView(LoginRequiredMixin, ListView):
    model = Package
    paginate_by = 10

    def get_queryset(self):
        return Package.objects.filter(Q(user=self.request.user) | Q(user=None)).order_by('-date_created')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        filtered_qs = PackageFilter(self.request.GET, self.get_queryset()).qs.order_by('-date_created')
        context['paginator'] = Paginator(filtered_qs, self.paginate_by)
        context['filter'] = PackageFilter(self.request.GET, queryset=filtered_qs)
        page = self.request.GET.get('page')
        if page is None:
            page = 1
        context['page_obj'] = context['paginator'].page(page)
        return context

def reappraise_package(package):
    ref_tests = test_reevaluate.objects.filter(package=package.id, is_reference=True)
    for ref_test in ref_tests:
        ref_test.images = []
        for image in ImageContainer.objects.filter(test=ref_test).iterator():
            image.answers = AnswerContainer.objects.filter(img=image)
            ref_test.images.append(image)
    package.reference_tests = ref_tests
    for test in Test.objects.filter(package=package.id, is_reference=False).iterator():
        reappraise_test(package, test)



def package_reevaluate(request, pk):
    if request.user.is_authenticated is False:
        return HttpResponseForbidden()
    package = get_object_or_404(Package, pk=pk)
    reappraise_package(package)

    success_url = '/package/' + str(pk)
    next_page = request.POST.get('next', success_url)
    return HttpResponseRedirect(next_page)


def package_pdf(request, pk):
    if request.user.is_authenticated is False or (get_object_or_404(Package, pk=pk).user != request.user and get_object_or_404(Package, pk=pk).user is not None):
        return HttpResponseForbidden()
    package = get_object_or_404(Package, pk=pk)
    buffer = io.BytesIO()
    with open(os.path.join(package.folder_path, "test.pdf"), "rb") as pdf:
        buffer.write(pdf.read())
        buffer.seek(io.SEEK_SET)
        return FileResponse(buffer, as_attachment=True, filename=package.name + '.pdf')

def package_raport(request, pk):
    if request.user.is_authenticated is False or (get_object_or_404(Package, pk=pk).user != request.user and sget_object_or_404(Package, pk=pk).user is not None):
        return HttpResponseForbidden()
    package = package = get_object_or_404(Package, pk=pk)
    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = 'attachment; filename="%s_raport.csv"' % package.name
    writer = csv.writer(response, delimiter=',')
    writer.writerow(['student_index', 'acquired_points'])
    for test in Test.objects.filter(package=package, is_reference=False):
        writer.writerow([test.student_index, test.acquired_points])
    return response
