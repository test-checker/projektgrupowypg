#!/bin/sh

#restart ftp
service proftpd restart

#monitor folder for ftp input changes
python3 -m src.ftp_input_watcher &

#run Web
./manage.py runserver 0.0.0.0:8000 --insecure
