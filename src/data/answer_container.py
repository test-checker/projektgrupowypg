class AnswerContainer:

    def __init__(self):
        self.acquired_points = 0
        self.evaluated_value = ""
        self.answer_id = 0
        self.x1 = 0.0
        self.x2 = 0.0
        self.y1 = 0.0
        self.y2 = 0.0

    def __str__(self):
        return str(self.answer_id) + " - Evaluated value:" + self.evaluated_value
