from .test import Test

class Package:
    STATUS_DICT = [
        ('processing', 'during evaluation'),
        ('error', 'failed to evaluate'),
        ('finished', 'package evaluated')
    ]

    SCORING_MODES = [
        ('basic', '1 if whole answer is correct'),
        ('sum', '1 for correct, -1 for incorrect'),
        ('sum_double', '2 for correct, -1 for incorrect')
    ]

    def __init__(self):
        self.folder_path = ""
        self.Name = ""
        self.reference_test = Test()
        self.tests = []
        self.allow_negative = False
        self.error_message = ""

    def __eq__(self, other):
        """Override the default Equals behavior"""
        return self.Name == other.Name and self.folder_path == other.folder_path
