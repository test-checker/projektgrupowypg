from src.net.models.index_model import IndexModel
from src.net.models.answer_model import AnswerModel
from src.preprocessing.data_extractor import get_index_column, extract_group_row, get_answer_row
import tensorflow as tf
from tensorflow import Graph

MODULE_NAME = "indexEvaluator"


class DNNApi:
    def __init__(self):
        self.graph_student_index = Graph()
        with self.graph_student_index.as_default():
            self.session_student_index = self.get_session()
            with self.session_student_index.as_default():
                self.CNN_student_index_model = IndexModel()

        self.graph_answer = Graph()
        with self.graph_answer.as_default():
            self.session_answer = self.get_session()
            with self.session_answer.as_default():
                self.CNN_answer_model = AnswerModel()

    @staticmethod
    def get_session():
        gpu_options = tf.compat.v1.GPUOptions(allow_growth=True)
        return tf.compat.v1.Session(config=tf.compat.v1.ConfigProto(gpu_options=gpu_options))

    def evaluate_index(self, img):
        # default parameters for neural network CNN_student_index_model
        required_height = 290
        required_width = 60
        index = ""
        # iterate over all columns
        with self.graph_student_index.as_default():
            with self.session_student_index.as_default():
                for img_index_column in get_index_column(img, required_height, required_width):
                    prediction = self.CNN_student_index_model.predict_digit(img_index_column)
                    index += str(prediction)
        return index

    def evaluate_answers(self, img):
        result = []
        threshold = 0.5

        with self.graph_answer.as_default():
            with self.session_answer.as_default():
                # iterate over all AnswerContainer class objects
                for answer_container, img in get_answer_row(img):
                    predict = self.CNN_answer_model.predict_answer(img)

                    # set threshold
                    predict[predict >= threshold] = 1
                    predict[predict < threshold] = 0

                    # decode answer from vector to characters
                    answer = decode_answers(predict.flatten())

                    answer_container.evaluated_value = answer
                    result.append(answer_container)

        return result

    def evaluate_group(self, img):

        with self.graph_answer.as_default():
            with self.session_answer.as_default():
                prediction = self.CNN_answer_model.predict_group(extract_group_row(img))
                return prediction



def decode_answers(encoded):
    answer = ""
    for i, value in enumerate(encoded):
        if value == 1:
            answer += str(chr(ord('A') + i))

    return answer
