import cv2
import os
import argparse
import src.img_normalization as ImgNormalization
import src.net.dnn_api as DNNApiMod
from src.preprocessing.rotation import display_image
from pdf2image import convert_from_path

def process_test(file):
    if os.path.isfile(file):
        name, ext = os.path.splitext(file)
        DNNApiObj = DNNApiMod.DNNApi()
        if ext == '.pdf':
            pages = convert_from_path(file)
            for page in pages:
                page.save(name + '.jpg', 'JPEG')
                img = cv2.imread(name + '.jpg')
                if img is not None:
                    img = ImgNormalization.normalize_image(img)
                    answers = DNNApiObj.evaluate_answers(img)
                    index = DNNApiObj.evaluate_index(img)
                    group = DNNApiObj.evaluate_group(img)
                    display_image("Final", img)
        elif ext == '.png' or ext == '.tiff' or ext == '.jpeg' or ext == '.jpg':
            img = cv2.imread(file)
            if img is not None:
                img = ImgNormalization.normalize_image(img)
                #display_image("Original", img)
                answers = DNNApiObj.evaluate_answers(img)
                index = DNNApiObj.evaluate_index(img)
                display_image("Final", img)
        else:
            print("Directory does not exist, exiting")
            exit()
    assert answers
    assert index
    print("Checked group is: " + group)
    for answer in answers:
        print(answer)
    print(index)


if __name__ == "__main__":
    #     # initiate the parser
    parser = argparse.ArgumentParser()

    # add long and short argument
    parser.add_argument("--file",
                        "-f",
                        help="path to file",
                        dest="file",
                        required=True)
    # read arguments from the command line
    args = parser.parse_args()

    if os.path.isfile(args.file):
        process_test(args.file)
    else:
        for file in os.listdir(args.file):
            process_test(args.file + '\\' + file)
