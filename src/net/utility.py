import tensorflow.keras.backend as K

#def Clear_backend():
#    tensorflow.keras.backend.clear_session()

# This is matric for exact match, it returns 1 only if all predictions matches true labels, if at least one answer is predicted wrongly with threshold 0.5, it will return 0
def macro_accuracy(y_true, y_pred):
    return K.min(K.cast(K.equal(y_true, K.round(y_pred)), dtype='float16'), axis=1)
