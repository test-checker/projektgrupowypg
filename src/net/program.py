import numpy as np
import argparse

from src.net.models.answer_model import AnswerModel
from src.net.dnn_api import decode_answers
from src.preprocessing.prepare_dataset import get_rows_dataset
from src.preprocessing.rotation import display_image

# TODO: remove this file

if __name__ == "__main__":

    # initiate the parser
    parser = argparse.ArgumentParser()

    # add long and short argument
    parser.add_argument("--source",
                    "-s",
                    help="path to image",
                    dest="source")

    parser.add_argument("--weights",
                    "-w",
                    help="path to weights",
                    dest="weights")

    parser.add_argument("--images",
                    "-i",
                    help="path to train images",
                    dest="images")

    parser.add_argument("--labels",
                    "-l",
                    help="path to labels",
                    dest="labels")

    parser.add_argument("--test_images",
                    "-ti",
                    help="path to train images",
                    dest="test_images")

    parser.add_argument("--test_labels",
                    "-tl",
                    help="path to labels",
                    dest="test_labels")

    # read arguments from the command line
    args = parser.parse_args()

    # shape of input data of neural network
    answers_shape = (60, 290)
    index_shape = (290, 60)

    image_shape = answers_shape

    # get dataset
    training_data = get_rows_dataset(args.images,
             args.labels,
             image_shape)

    test_data = get_rows_dataset(args.test_images,
                                 args.test_labels,
                                 image_shape)

    # load model
    #model = CNN_student_index(image_shape + (3,))
    # model = model.CNN_answer(image_shape + (3,))
    model = AnswerModel(False)
    model.train_model(training_data, image_shape + (3,), args.weights, epochs=150, batch_size=32)

    # model.load_weights(args.weights)

    model.test_model(*test_data) #rozwija tuple

    # for i, (x_test, y_test) in enumerate(zip(test_data[0], test_data[1])):
    #     prediction = model.predict_answer( np.array( [x_test,] ))
    #     prediction[prediction >= 0.5] = 1
    #     prediction[prediction < 0.5] = 0

    #     # decode answer from vector to characters
    #     answer = decode_answers(prediction.flatten())
    #     label = decode_answers(y_test.flatten())
    #     if answer != label:
    #         print("Index of prediction: " + str(i) + ", prediction: " + answer + ", label: " + label)
    #         display_image("Failed", x_test)
    #evaluate_answers(args.source, args.weights)
    #evaluate_index(args.source, args.weights)
