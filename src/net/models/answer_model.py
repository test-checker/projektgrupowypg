import os
import tensorflow
import datetime
import numpy as np
import argparse
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import Activation
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import MaxPooling2D
from sklearn.model_selection import train_test_split
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from src.net.utility import macro_accuracy
from src.preprocessing.prepare_dataset import get_rows_dataset
from src.preprocessing.rotation import display_image



class AnswerModel:

    def __init__(self, from_file=True) -> None:
        input_shape = (60, 290, 3)
        self.model = self.load_model() if from_file else self.init_model(input_shape)

    def init_model(self, input_shape, learning_rate=1e-5):
        model = Sequential()
        model.add(Conv2D(32, (3, 3), padding='same',
                        input_shape=input_shape))
        model.add(Activation('relu'))
        model.add(Conv2D(32, (3, 3)))
        model.add(Activation('relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))

        model.add(Conv2D(64, (3, 3), padding='same'))
        model.add(Activation('relu'))
        model.add(Conv2D(64, (3, 3)))
        model.add(Activation('relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))

        model.add(Conv2D(128, (3, 3), padding='same'))
        model.add(Activation('relu'))
        model.add(Conv2D(128, (3, 3)))
        model.add(Activation('relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))

        model.add(Conv2D(256, (3, 3), padding='same'))
        model.add(Activation('relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))

        model.add(Flatten())
        model.add(Dense(2048))
        model.add(Activation('relu'))
        model.add(Dropout(0.225))

        model.add(Dense(1024))
        model.add(Activation('relu'))
        model.add(Dropout(0.215))

        model.add(Dense(712))
        model.add(Activation('relu'))
        model.add(Dropout(0.205))
        model.add(Dense(4))
        model.add(Activation('sigmoid'))

        opt = tensorflow.keras.optimizers.Adam(learning_rate=learning_rate)
        # TODO: spróbuj tego:
        # decay = learning_rate / epochs
        # momentum = 0.8
        #opt = tensorflow.keras.optimizers.SGD(learning_rate=learning_rate*10, momentum=momentum, decay=decay) #  może jeszcze nesterov=False

        model.compile(loss='binary_crossentropy',
                    optimizer=opt,
                    metrics=['binary_accuracy', macro_accuracy])
        return model


    def load_model(self):
        dependencies = {
            'macro_accuracy': macro_accuracy
        }
        self.model = tensorflow.keras.models.load_model('./keras_models/CNN_answer_full', custom_objects=dependencies)
        return self.model

    def save_model(self):
        self.model.load_weights(os.path.join(os.path.dirname(os.path.abspath(__file__)), "answer.weights"))
        self.model.save('./keras_models/CNN_answer_full')

    def predict_answer(self, input):
        print("--------- Answer prediction")
        print(self.model.predict(input))
        return self.model.predict(input)

    def predict_group(self, input):
        predict = self.model.predict(input)
        prediction = np.argmax(predict)
        if np.take(predict, prediction) < 0.5:
            return 'X'
        return str(chr(ord('A') + prediction))

    def train_model(self, training_data, input_shape, weights_filename, epochs=800, batch_size=8, test_size=0.2):
        # split to train and test data
        X_train, X_val, y_train, y_val = train_test_split(*training_data, test_size=test_size)

        # create train data generator
        datagen = ImageDataGenerator(
            featurewise_center=False,
            featurewise_std_normalization=False,
            rotation_range=3,
            width_shift_range=0.2,
            height_shift_range=0.2,
            horizontal_flip=False)
            #brightness_range=[0.8, 1.2])

        datagen.fit(X_train)

        # create test data generator
        test_datagen = ImageDataGenerator()

        test_datagen.fit(X_val)


        #model.load_weights(weights_filename)

        #tensorboard callbacks
        log_dir = "logs/fit/" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
        tensorboard_callback = tensorflow.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=0)

        self.model.fit(datagen.flow(X_train, y_train, batch_size=batch_size),
                        steps_per_epoch=len(X_train) / batch_size,
                        epochs=epochs,
                        validation_data=test_datagen.flow(X_val, y_val, batch_size=batch_size),
                        shuffle=True,
                        callbacks=[tensorboard_callback])

        self.model.save_weights(weights_filename)

        print(self.model.evaluate(X_val, y_val))

    def test_model(self, input_data, output_data):
        score = self.model.evaluate(x=input_data, y=output_data)
        print('---------------------')
        print('Answer model results:')
        print('Loss: ', score[0])
        print('Accuracy: ', score[1])
        print('---------------------')



if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    # add long and short argument
    parser.add_argument("--source",
        "-s",
        help="path to image",
        dest="source")

    parser.add_argument("--weights",
                    "-w",
                    help="path to weights",
                    dest="weights")

    parser.add_argument("--images",
                    "-i",
                    help="path to train images",
                    dest="images")

    parser.add_argument("--labels",
                    "-l",
                    help="path to labels",
                    dest="labels")

    parser.add_argument("--test_images",
                    "-ti",
                    help="path to train images",
                    dest="test_images")

    parser.add_argument("--test_labels",
                    "-tl",
                    help="path to labels",
                    dest="test_labels")

    # read arguments from the command line
    args = parser.parse_args()

    # shape of input data of neural network
    answers_shape = (60, 290)

    image_shape = answers_shape

    # get dataset
    # training_data = get_rows_dataset(args.images,
    #         args.labels,
    #         image_shape)

    test_data = get_rows_dataset(args.test_images,
                                 args.test_labels,
                                 image_shape)
    model = AnswerModel()

    if args.weights:
        model.test_model(test_data[0], test_data[1])
