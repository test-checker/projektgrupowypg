import os
import tensorflow
import numpy as np
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Dense
from tensorflow.keras.layers import Dropout
from tensorflow.keras.layers import Activation
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import MaxPooling2D
from src.net.utility import macro_accuracy


class IndexModel:
    def __init__(self, from_file=True) -> None:
        input_shape = (290, 60, 3)
        self.model = self.load_model() if from_file else self.init_model(input_shape)

    def init_model(self, input_shape, learning_rate=1e-5):
        model = Sequential()
        model.add(Conv2D(32, (3, 3), padding='same',
                        input_shape=input_shape))
        model.add(Activation('relu'))
        model.add(Conv2D(32, (3, 3)))
        model.add(Activation('relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))

        model.add(Conv2D(64, (3, 3), padding='same'))
        model.add(Activation('relu'))
        model.add(Conv2D(64, (3, 3)))
        model.add(Activation('relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))

        model.add(Conv2D(128, (3, 3), padding='same'))
        model.add(Activation('relu'))
        model.add(Conv2D(128, (3, 3)))
        model.add(Activation('relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))

        model.add(Conv2D(256, (3, 3), padding='same'))
        model.add(Activation('relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))

        model.add(Flatten())
        model.add(Dense(1024))
        model.add(Activation('relu'))
        model.add(Dropout(0.25))

        model.add(Dense(712))
        model.add(Activation('relu'))
        model.add(Dropout(0.225))
        model.add(Dense(11))
        model.add(Activation('softmax'))

        optimizer = tensorflow.keras.optimizers.Adam(learning_rate=learning_rate)
        model.compile(loss='categorical_crossentropy',
                    optimizer=optimizer,
                    metrics=['binary_accuracy', macro_accuracy])
        return model

    def load_model(self):
        dependencies = {
            'macro_accuracy': macro_accuracy
        }
        return tensorflow.keras.models.load_model('./keras_models/CNN_student_index_full', custom_objects=dependencies)

    def save_model(self):
        self.model.load_weights(os.path.join(os.path.dirname(os.path.abspath(__file__)), "index.weights"))
        self.model.save('./keras_models/CNN_student_index_full')

    def predict_digit(self, input):
        prediction = np.argmax(self.model.predict(input))
        if prediction == 0: #no answer was selected
            prediction = "_"
        else:
            prediction = prediction % 10
        return prediction

    def test_model(self):
        score = self.model.evaluate(x=input_data, y=output_data)
        print('---------------------')
        print('Index model results:')
        print('Loss: ', score[0])
        print('Accuracy: ', score[1])
        print('---------------------')


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    # add long and short argument
    parser.add_argument("--source",
        "-s",
        help="path to image",
        dest="source")

    parser.add_argument("--weights",
                    "-w",
                    help="path to weights",
                    dest="weights")

    parser.add_argument("--images",
                    "-i",
                    help="path to train images",
                    dest="images")

    parser.add_argument("--labels",
                    "-l",
                    help="path to labels",
                    dest="labels")

    parser.add_argument("--test_images",
                    "-ti",
                    help="path to train images",
                    dest="test_images")

    parser.add_argument("--test_labels",
                    "-tl",
                    help="path to labels",
                    dest="test_labels")

    # read arguments from the command line
    args = parser.parse_args()

    # shape of input data of neural network
    index_shape = (290, 60)

    image_shape = index_shape

    # get dataset
    # training_data = get_rows_dataset(args.images,
    #         args.labels,
    #         image_shape)

    test_data = get_rows_dataset(args.test_images,
                                 args.test_labels,
                                 image_shape)

    # load model
    #model = CNN_student_index(image_shape + (3,))
    # model = model.CNN_answer(image_shape + (3,))
    model = IndexModel()
    # model.train_model(training_data, image_shape + (3,), args.weights, epochs=150, batch_size=32)

    # model.load_weights(args.weights)

    #results = model.evaluate(*test_data) #rozwija tuple
    #print('test loss, test acc, macro_accuracy:', results)

    """
    for i, (x_test, y_test) in enumerate(zip(test_data[0], test_data[1])):
        prediction = model.predict_answer( np.array( [x_test,] ))
        prediction[prediction >= 0.5] = 1
        prediction[prediction < 0.5] = 0

        # decode answer from vector to characters
        answer = decode_answers(prediction.flatten())
        label = decode_answers(y_test.flatten())
        if answer != label:
            print("Index of prediction: " + str(i) + ", prediction: " + answer + ", label: " + label)
            display_image("Failed", x_test)
    #evaluate_answers(args.source, args.weights)
    #evaluate_index(args.source, args.weights)
    """
    if args.weights:
        model.test_model(test_data[0], test_data[1])
