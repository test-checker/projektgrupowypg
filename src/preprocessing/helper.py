import cv2


def draw_overlay(img, rectangles):
    copy_img = img.copy()
    for (p1,p2,p3,p4) in rectangles:
        cv2.rectangle(copy_img, (p1,p2), (p3,p4), (230,230,230), 10)
    show_image(copy_img)

def show_image(img):
    if img.shape[0] > 400 or img.shape[1] > 400:
        scale_percent = 40

        # calculate the 50 percent of original dimensions
        width = int(img.shape[1] * scale_percent / 100)
        height = int(img.shape[0] * scale_percent / 100)

        # dsize
        dsize = (width, height)

        # resize image
        img = cv2.resize(img, dsize)
    cv2.imshow('image', img)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
