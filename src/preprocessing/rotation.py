import sys

import numpy as np
import cv2
from math import atan2
from math import degrees


def rotate_image(image, angle):
    height = image.shape[0]
    width = image.shape[1]
    height_big = height * 2
    width_big = width * 2
    image_big = cv2.resize(image, (width_big, height_big))
    image_center = (width_big / 2, height_big / 2)

    # rotation center
    rot_mat = cv2.getRotationMatrix2D(image_center, angle, 0.5)
    result = cv2.warpAffine(image_big, rot_mat, (width_big, height_big), flags=cv2.INTER_LINEAR)

    new_height = result.shape[0]
    new_width = result.shape[1]
    new_image_center = (new_height / 2, new_width / 2)
    if height > width:
        result = result[int(new_image_center[0] - (height / 2)):int(new_image_center[0] + (height / 2)),
                 int(new_image_center[1] - (width / 2)):int(new_image_center[1] + (width / 2))]
    else:
        result = result[int(new_image_center[0] - (width / 2)):int(new_image_center[0] + (width / 2)),
                 int(new_image_center[1] - (height / 2)):int(new_image_center[1] + (height / 2))]
    return result


def detect_marker_points(image, filter_size=91, starting_threshold=120):
    img2 = cv2.GaussianBlur(image, (filter_size, filter_size), 0)
    img_gray = cv2.cvtColor(img2, cv2.COLOR_BGR2GRAY)
    # show_image(img_gray)
    current_threshold = starting_threshold
    cont = []
    min_distance = 0
    while current_threshold < 200 and (len(cont) < 6 or min_distance < 100.0):
        _, threshold = cv2.threshold(img_gray, current_threshold, 255, cv2.THRESH_BINARY)
        # show_image(threshold)
        cont, _ = cv2.findContours(threshold, cv2.RETR_LIST, cv2.CHAIN_APPROX_SIMPLE)
        # DEBUG FUNCTION, DON'T REMOVE, IT SHOWS DETECTED CENTROIDS
        # if len(contours) > 6:
        #     copy_img = img_gray.copy()
        #     for contour in contours:
        #         rect = cv2.minAreaRect(contour)
        #         try:
        #             cv2.putText(copy_img, str(cv2.contourArea(contour)), (int(rect[0][0]), int(rect[0][1])), cv2.FONT_HERSHEY_COMPLEX, 1, (255, 20, 147), 2)
        #         except Exception as e:
        #             print(e)
        #     cv2.drawContours(copy_img, contours, -1, (0, 255, 0), 3)
        #     show_image(copy_img)
        if len(cont) > 6:
            cont = eliminate_additional_contours(cont)
            centroids = detect_centroids(cont)
            min_distance = calculate_min_distance(centroids)
        # DEBUG FUNCTION, DON'T REMOVE, IT SHOWS CLASSIFIED AS CORRECT CENTROIDS
        # copy_img = img_gray.copy()
        # for contour in contours:
        #     rect = cv2.minAreaRect(contour)
        #     try:
        #         cv2.putText(copy_img, str(cv2.contourArea(contour)), (int(rect[0][0]), int(rect[0][1])), cv2.FONT_HERSHEY_COMPLEX, 1, (150, 150, 150), 2)
        #     except Exception as e:
        #         print(e)
        # cv2.drawContours(copy_img, contours, -1, (0, 255, 0), 3)
        # show_image(copy_img)
        current_threshold += 10
    return cont


def calculate_min_distance(centroids):
    min_distance = 0.0
    for centroid_1 in centroids:
        for centroid_2 in centroids:
            if centroid_1 == centroid_2:
                continue
            (xA, yA) = (centroid_1[0], centroid_1[1])
            (xB, yB) = (centroid_2[0], centroid_2[1])
            distance = ((xB-xA)**2+(yB-yA)**2)**(1/2) # distance formula
            if distance < min_distance or min_distance == 0.0:
                min_distance = distance
    return min_distance

def detect_centroids(contours, epsilon=1000):
    centroids = []

    # loop over the contours
    for i, p in enumerate(contours):
    # compute the center of the contour
        M = cv2.moments(p)

        if M["m00"] == 0:
            continue

        c_x = int(M["m10"] / M["m00"])
        c_y = int(M["m01"] / M["m00"])

        centroids.append([c_x, c_y])
    return centroids


def choose_edge_points(centroids):
    min_x = min_y = np.Inf
    max_x = max_y = 0
    for c in centroids:
        if c[0] > max_x:
            max_x = c[0]
        if c[0] < min_x:
            min_x = c[0]
        if c[1] > max_y:
            max_y = c[1]
        if c[1] < min_y:
            min_y = c[1]
    return min_x, max_x, min_y, max_y


def are_collinear(p0, p1, p2, min_x, max_x, min_y, max_y, epsilon=1e4, threshold=100):
    contains = (p0[0] == min_x or p0[0] == min_x or p1[0] == max_x or \
                p1[0] == min_x or p2[0] == max_x or p2[0] == min_x) and \
               (p0[1] == min_y or p0[1] == min_y or p1[1] == max_y or \
                p1[1] == min_y or p2[1] == max_y or p2[1] == min_y)
    in_threshold = abs(p0[1] - p1[1]) < threshold and \
                   abs(p0[1] - p2[1]) < threshold and \
                   abs(p2[1] - p1[1]) < threshold
    if contains and in_threshold:
        x1, y1 = p1[0] - p0[0], p1[1] - p0[1]
        x2, y2 = p2[0] - p0[0], p2[1] - p0[1]
        return abs(x1 * y2 - x2 * y1) < epsilon
    else:
        return False

def calculate_slice(row, column, min_x, min_y, max_x, max_y):
    if column > 2 or column < 0 or row > 2 or row < 0:
        raise RuntimeError("Wrong argument! image slice/row is not in range <0,2>")
    width = max_x - min_x
    height = max_y - min_y
    return (min_x + (int)(column*width/3), (int)(min_y + row*height/3), min_x + (int)((column + 1)*width/3), min_y + (int)((row + 1)*height/3))

def get_collinear_points(centroids, min_x, max_x, min_y, max_y):
    #let's divide page into 9 areas in a following way:
    #      |     |    #
    #(0,0) |(0,1)|..  #
    #------+-----+----#
    #      |     |    #
    #(1,0) | ..  |..  #
    #------+-----+----#
    #      |     |    #
    #(2,0) | ..  |..  #
    slices = []
    for row in range (0,3):
        slices.append([])
        for column in range (0,3):
            slices[row].append(calculate_slice(row,column,min_x,min_y,max_x,max_y))
    # Check first and last row, cause it's the only viable place to find 3 horizontal centroids
    for row in [0, 2]:
        centroids_found = [None, None, None]
        for centroid in centroids:
            if not slices[row][column][1] <= centroid[1] or not centroid[1] <= slices[row][column][3]:
                continue
            for column in range(0,3):
                if slices[row][column][0] <= centroid[0] <= slices[row][column][2] and slices[row][column][1] <= centroid[1] <= slices[row][column][3] + 1:
                    centroids_found[column] = centroid
        if all(centroids_found):
            return tuple(centroids_found)
    raise RuntimeError("No colinear centroids found")

def get_collinear_points_old(centroids, min_x, max_x, min_y, max_y):
    for p0 in centroids:
        for p1 in centroids:
            if p1 is p0:
                continue
            for p2 in centroids:
                if p2 is p0 or p2 is p1:
                    continue
                if are_collinear(p0, p1, p2, min_x, max_x, min_y, max_y):
                    return p0, p1, p2


def get_required_rotation(min_x, max_x, min_y, max_y, p0, p1, p2, epsilon=5):
    rotation = 0

    # check if each point's dimension equals to min/max coordinate with given
    # threshold to determine position of image
    p0_min_x = (min_x - epsilon < p0[0] and p0[0] < min_x + epsilon)
    p0_max_x = (max_x - epsilon < p0[0] and p0[0] < max_x + epsilon)
    p0_min_y = (min_y - epsilon < p0[1] and p0[1] < min_y + epsilon)
    p0_max_y = (max_y - epsilon < p0[1] and p0[1] < max_y + epsilon)
    p1_min_x = (min_x - epsilon < p1[0] and p1[0] < min_x + epsilon)
    p1_max_x = (max_x - epsilon < p1[0] and p1[0] < max_x + epsilon)
    p1_min_y = (min_y - epsilon < p1[1] and p1[1] < min_y + epsilon)
    p1_max_y = (max_y - epsilon < p1[1] and p1[1] < max_y + epsilon)
    p2_min_x = (min_x - epsilon < p2[0] and p2[0] < min_x + epsilon)
    p2_max_x = (max_x - epsilon < p2[0] and p2[0] < max_x + epsilon)
    p2_min_y = (min_y - epsilon < p2[1] and p2[1] < min_y + epsilon)
    p2_max_y = (max_y - epsilon < p2[1] and p2[1] < max_y + epsilon)

    degree = get_angle_between_2_points(p0, p2)

    if (p0_min_y or p1_min_y or p2_min_y) and (p0_min_x or p1_min_x or p2_min_x) and degree <= 45:
        rotation = 270 - (90 + degree)
    elif (p0_min_y or p1_min_y or p2_min_y) and (p0_min_x or p1_min_x or p2_min_x) and degree > 45:
        rotation = 180 - (90 + degree)
    elif (p0_min_y or p1_min_y or p2_min_y) and (p0_max_x or p1_max_x or p2_max_x) and degree <= 45:
        rotation = 270 - (90 - degree)
    elif (p0_min_y or p1_min_y or p2_min_y) and (p0_max_x or p1_max_x or p2_max_x) and degree > 45:
        rotation = 180 - (90 - degree)
    elif (p0_max_y or p1_max_y or p2_max_y) and (p0_min_x or p1_min_x or p2_min_x) and degree <= 45:
        rotation = 90 - (90 + degree)
    elif (p0_max_y or p1_max_y or p2_max_y) and (p0_min_x or p1_min_x or p2_min_x) and degree > 45:
        rotation = 90 - (90 - degree)
    elif (p0_max_y or p1_max_y or p2_max_y) and (p0_max_x or p1_max_x or p2_max_x) and degree <= 45:
        rotation = 360 - degree
    elif (p0_max_y or p1_max_y or p2_max_y) and (p0_max_x or p1_max_x or p2_max_x) and degree > 45:
        rotation = 0 - (90 + degree)

    return abs(rotation)


def get_angle_between_2_points(p1, p2):
    x_diff = abs(p2[0] - p1[0])
    y_diff = abs(p2[1] - p1[1])
    return degrees(atan2(y_diff, x_diff))


def display_image(name, image):
    cv2.namedWindow(name, cv2.WINDOW_NORMAL)
    cv2.imshow(name, image)
    cv2.resizeWindow(name, 600, 600)
    cv2.waitKey(10000)
    cv2.destroyAllWindows()


def center_image_due_to_marker_points(img):
    # get contours of marker points
    contours = detect_marker_points(img)

    if len(contours) != 6:
        # show_image(img)
        raise Exception("Can't determine correct contours, all contours number: " + str(len(contours)))

    # get list of points representing marker points centroids
    centroids = detect_centroids(contours)

    if not centroids:
        raise Exception('Cannot detect centroids')

    #DEBUG FUNCTION, DON'T REMOVE, IT SHOWS DETECTED CENTROIDS
    # rectangles = []
    # for centroid in centroids:
    #     rectangles.append((centroid[0]-20, centroid[1]-20, centroid[0]+20, centroid[1]+20))
    # draw_overlay(img, rectangles)

    # calculate minimum and maximum coordinates in each dimension
    min_x, max_x, min_y, max_y = choose_edge_points(centroids)
    #DEBUG FUNCTION, DON'T REMOVE, IT SHOWS DETECTED CENTROIDS
    # rectangles = []
    # rectangles.append((min_x, min_y, max_x, max_y))
    # draw_overlay(img, rectangles)

    # get coordinates of 3 marker points (horizontal line)
    p0, p1, p2 = get_collinear_points(centroids, min_x, max_x, min_y, max_y)

    # calculate rotation based on marker points
    rotation = get_required_rotation(min_x, max_x, min_y, max_y, p0, p1, p2)

    return rotate_image(img, rotation)

def eliminate_additional_contours(contours: list):
    # Eliminates contour of whole page
    sorted_contour_sizes = sorted([cv2.contourArea(contour) for contour in contours])
    sorted_contour_sizes.reverse()
    if sorted_contour_sizes[0] > 50000.0:
        target_size = sorted_contour_sizes[6]
    else:
        target_size = sorted_contour_sizes[5]
    contours_to_return = []
    for contour in contours:
        if cv2.contourArea(contour) >= target_size and cv2.contourArea(contour) < 50000.0:
            contours_to_return.append(contour)
    return contours_to_return



def trim_image_to_marker_points(img):
    # get contours of marker points
    contours = detect_marker_points(img)

    if len(contours) != 6:
        raise Exception("Can't determine correct contours, all contours number: " + str(len(contours)))

    # get list of points representing marker points centroids
    centroids = detect_centroids(contours)

    if not centroids:
        raise Exception('Cannot detect centroids')

    # calculate minimum and maximum coordinates in each dimension
    min_x, max_x, min_y, max_y = choose_edge_points(centroids)

    # trim image to marker area
    return img[min_y:max_y, min_x:max_x]


def center_and_trim_image(img):
    # center image due to marker points
    img = center_image_due_to_marker_points(img)

    # trim image
    img = trim_image_to_marker_points(img)
    return img
