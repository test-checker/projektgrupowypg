from src.preprocessing.rotation import center_and_trim_image


def normalize_image(img):
    # normalize image to further processing
    img = center_and_trim_image(img)
    return img
