import errno
import os


def create_directory(name):
    if not os.path.exists(os.path.dirname(name)):
        try:
            os.makedirs(os.path.dirname(name))
        except OSError as exc:
            if exc.errno != errno.EEXIST:
                raise
