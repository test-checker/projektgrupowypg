#!/bin/sh

./manage.py makemigrations
./manage.py migrate
./manage.py parametered_createsuperuser --username $USER_LOGIN --password $USER_PASSWORD --noinput --email 'blank@email.com'
